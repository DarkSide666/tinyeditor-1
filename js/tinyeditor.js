(function($){
	$.each({
        tinyeditor: function(obj) {
        	tinymce.init({
			    selector: '#'+obj,
			    plugins: [
				"advlist autolink link image lists charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
				"table contextmenu directionality emoticons paste textcolor filemanager"
				],
				image_advtab: true,
				toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code"
			});
			var form=$('#'+obj).parents('div.atk-form');
			
			form.on('beforesubmit',function(){
				var id=$(this).find('#'+obj).attr('id');
				var text=tinymce.get(id).getContent();
				$('#'+id).val(text);
			})

        }
	},$.univ._import);

})($);