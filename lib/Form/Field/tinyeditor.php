<?php
namespace tinyeditor;
class Form_Field_tinyeditor extends \Form_Field_Text {
	function init() {
		parent::init();
		
		// add add-on locations to pathfinder
		$l = $this->api->locate('addons', __NAMESPACE__, 'location');
		$addon_location = $this->api->locate('addons', __NAMESPACE__);
		$this->api->pathfinder->addLocation($addon_location, array(
			'js' => 'js',
		))->setParent($l);
		// Load and init tinymce
		$this->api->jui->addStaticInclude('tinymce/tinymce.min');
		$this->api->js(true)->_load('tinyeditor');
		$this->js(true)->addClass('tinyeditor')->univ()->tinyeditor($this->name);
	}
	
}